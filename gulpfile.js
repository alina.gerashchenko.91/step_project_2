const {
  series,
  parallel,
  watch,
  src,
  dest
} = require("gulp");

const browserSync = require("browser-sync").create();
const sass = require('gulp-sass')(require('sass'));
const minifyjs = require('gulp-js-minify');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const imagemin = require('gulp-imagemin');
const htmlImport = require('gulp-html-import');

const serv = () => {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
};

const bsReload = (cb) => {
  browserSync.reload();
  cb();
};

const html = () => {
  return src('./demo/index.html')
.pipe(htmlImport('./demo/components/'))
.pipe(dest('./'))
.pipe(browserSync.reload({stream: true}));
};


const styles = () => {
  return src("./src/scss/style.scss")
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS())
    .pipe(concat('styles.min.css'))
    .pipe(dest("./dist/styles/"))
    .pipe(browserSync.stream());
};

const js = () => {
  return src('./src/js/index.js')
    .pipe(minifyjs())
    .pipe(concat('scripts.min.js'))
    .pipe(dest("./dist/js/"))
    .pipe(browserSync.stream());
};


const img = () => {
  return src("./src/img/**/*.*")
    .pipe(imagemin(({
      optimizationLevel: 3
    })))
    .pipe(dest('./dist/img/'))
    .pipe(browserSync.stream());
};

const clear = () => {
  return del('./dist/**/*.*');
}
// exports.clear = clear;

const watcher = (cb) => {
  watch("./index.html", bsReload);
  watch("./demo/*.html", html);
  watch("./src/scss/*.scss", styles);
  watch("./src/js/index.js", js);
  watch("./src/img/*.png", img);
  cb();
};

exports.dev = parallel(serv, watcher, series(html,styles,js));
exports.build = series(clear, html, styles, js, img);
